package imp.stellaris

import imp.logback.initLogbackCreateConfigIfMissing
import imp.stellaris.portrait.ImageConverter
import imp.stellaris.portrait.PortraitModGenerator
import imp.util.assertWorkingDirContains
import java.nio.file.Path
import java.nio.file.Paths
import kotlin.io.path.Path
import kotlin.system.exitProcess


private val appDir = Path("")
/**The directory that contains src, etc.*/
fun thisAppDir(): Path = appDir

private class Main


fun main(args: Array<String>) {
	if(args.size != 2) {
		println("Must provide exactly 2 arguments: path to the input dir containing archetypes, and path to the output mod dir (containing common and gfx). Found ${args.size} args.")
		exitProcess(0)
	}
	val inputDir = Paths.get(args[0])
	val outputDir = Paths.get(args[1])

	assertWorkingDirContains("src/main/kotlin")
	initLogbackCreateConfigIfMissing(Path("logback.xml"), "/logback-default.xml", Main::class)
	ImageConverter.setup()
	PortraitModGenerator.process(inputDir, outputDir)
}