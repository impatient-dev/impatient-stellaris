package imp.stellaris.portrait

import imp.stellaris.thisAppDir
import imp.util.*
import java.nio.file.Files
import java.nio.file.Path
import java.util.concurrent.TimeUnit

private val log = ImageConverter::class.logger

/**Not thread-safe. Must be setup before you can do anything else*/
object ImageConverter {
	private lateinit var magickLocation: String

	/**Finds ImageMagick, or throws an exception if it cannot b e located.*/
	fun setup() {
		val pathA = thisAppDir().resolve("bin").resolve("magick")
		val pathB = thisAppDir().resolve("bin").resolve("magick.exe")

		val path: Path = when {
			Files.exists(pathA) -> pathA
			Files.exists(pathB) -> pathB
			else -> throw Exception("ImageMagick executable must be placed at $pathA or $pathB")
		}

		//test ImageMagick
		val args = listOf(path.toString(), "--version")
		log.kinfo { "Testing ImageMagick via ${args.joinToString(" ")}" }
		try {
			ProcessBuilder(args).impRun { process ->
				if(!process.waitFor(10, TimeUnit.SECONDS)) {
					process.destroyForcibly()
					throw Exception("ImageMagick test took too long.")
				}

				val outStr = process.inputStream.availableToString()
				val errStr = process.errorStream.availableToString()
				log.debug("ImageMagick normal output: {}", outStr)
				log.debug("ImageMagick error output (should be empty): {}", errStr)
				if(!outStr.contains("Version") || !outStr.contains("ImageMagick") || !errStr.isBlank())
					throw Exception("Bad output from ImageMagick.")
				magickLocation = path.toString()
				log.debug("ImageMagick test success.")
			}
		} catch(e: Exception) {
			throw Exception("ImageMagick testing failed.", e)
		}
	}


	fun execute(task: ImageConvertTask, job: PortraitJob) {
		if(!this::magickLocation.isInitialized)
			throw IllegalStateException("Must be setup first.")
		val toPath = job.outModDir.resolve(task.dest)
		if(Files.exists(toPath)) {
			log.debug("Skipping because destination file exists: {} --> {}", task.src, task.dest)
			return
		}

		val args: List<String> = listOf(magickLocation, job.inDir.resolve(task.src).toString(), "-resize", "x350", toPath.toString())
		log.kdebug { "Executing ${args.joinToString(" ")}" }
		Files.createDirectories(toPath.parent) //ImageMagick won't do this

		try {
			ProcessBuilder(args).impRun {proc ->
				if(!proc.waitFor(30, TimeUnit.SECONDS))
					throw Exception("Image conversion took too long.")
				val code = proc.exitValue()
				if(code != 0) {
					log.error("ImageMagick failed with code {}", code)
					log.error("ImageMagick output: {}", proc.inputStream.availableToString())
					log.error("ImageMagick error output: {}", proc.errorStream.availableToString())
					throw Exception()
				}
				log.trace("Image conversion succeeded.")
			}
		} catch(e: Exception) {
			throw Exception("Failed to convert image ${task.src} to ${task.dest}")
		}
	}
}