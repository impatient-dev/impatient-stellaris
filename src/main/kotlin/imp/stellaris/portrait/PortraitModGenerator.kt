package imp.stellaris.portrait

import imp.stellaris.stripLastExtension
import imp.util.logger
import java.nio.file.Path
import java.nio.file.Paths

private val log = PortraitModGenerator::class.logger
private val imagesOutputDirRelative = Paths.get("gfx", "models", "portraits")

object PortraitModGenerator {
	fun process(inputDir: Path, outModDir: Path) {
		val job = PortraitJob(inputDir, outModDir)
		val handler = InputHandler(job)
		PortraitDirReader.processArchetypesDir(job.inDir, handler)

		val classesFileTask = SpeciesClassesFileTask (
			Paths.get("common", "species_classes", "species_classes_${job.modName}.txt"),
			handler.speciesClassesByName.values.sortedBy { it.name }
		)
		StellarisModFilesWriter.generate(classesFileTask, job)

		for(speciesClass in handler.speciesClassesByName.values) {
			for(portraitGroup in speciesClass.portraitGroupsByName.values) {
				val portraitTask = PortraitFileTask(portraitGroup, Paths.get("gfx", "portraits", "portraits", "portraits_${portraitGroup.name}.txt"))
				StellarisModFilesWriter.generate(portraitTask, job)
			}
		}

		StellarisModFilesWriter.generateModDescriptorFiles(job)
		log.info("Mod generated successfully: {}", job.outModDir)
	}
}


private class InputHandler (
	val job: PortraitJob,
) : InputPortraitFileHandler {
	val speciesClassesByName = HashMap<String, SpeciesClassBasicInfo>()

	override fun onImage(inputPath: Path, portraitGroup: String, speciesClass: String, speciesArchetype: String) {
		val srcFileName = inputPath.fileName.toString()
		val nameNoExtension = srcFileName.stripLastExtension()
		val tgtFileName = nameNoExtension + ".dds"

		//convert
		val outputRelativePath = imagesOutputDirRelative.resolve(tgtFileName)
		val imageTask = ImageConvertTask(inputPath, outputRelativePath)
		ImageConverter.execute(imageTask, job)

		//add to species classes datastructure
		val objCls = speciesClassesByName.computeIfAbsent(speciesClass) {SpeciesClassBasicInfo(speciesClass, speciesArchetype)}
		val objPorGrp = objCls.portraitGroupsByName.computeIfAbsent(portraitGroup) {PortraitGroupBasicInfo(portraitGroup)}
		objPorGrp.phenotypes.add(PortraitPhenotypeBasicInfo(nameNoExtension, inputPath, outputRelativePath))
	}
}
