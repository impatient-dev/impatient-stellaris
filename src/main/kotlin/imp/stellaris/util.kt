package imp.stellaris


/**Removes the file extension (the last period and everything after it) if there is one.
 * If there are multiple file extensions (foo.tar.gz), removes only the last one (.gz).*/
fun String.stripLastExtension(): String {
	val i = this.lastIndexOf('.')
	return if(i == -1) this else this.substring(0, i)
}