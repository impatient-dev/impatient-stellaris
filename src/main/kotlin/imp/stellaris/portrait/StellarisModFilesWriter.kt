package imp.stellaris.portrait

import imp.util.logger
import java.io.BufferedWriter
import java.io.FileWriter
import java.nio.file.Files

private val log = StellarisModFilesWriter::class.logger

private val modFile = CachedStringResource("/descriptor-template.mod")
private val speciesClassesFile = CachedStringResource("/species_classes-template-repeatable.txt")
private val speciesPortraitsFile = CachedStringResource("/species_portraits-template.txt")
private val speciesPortraitsFragment = CachedStringResource("/portrait-fragment-template.txt")

object StellarisModFilesWriter {
	fun generateModDescriptorFiles(job: PortraitJob) {
		val content = StellarisFileString(modFile.get())
		content.replaceStr("ModName", job.modName)
		val paths = listOf(
			job.outModDir.parent.resolve("${job.modName}.mod"),
			job.outModDir.resolve("descriptor.mod")
		)

		for(file in paths) {
			log.info("Generating {}", file)
			Files.createDirectories(file.parent)
			Files.deleteIfExists(file)
			BufferedWriter(FileWriter(file.toFile())).use { out ->
				out.write(content.toString())
			}
		}
	}


	fun generate(task: SpeciesClassesFileTask, job: PortraitJob) {
		val file = job.outModDir.resolve(task.path)
		log.info("Generating {}", file)
		Files.createDirectories(file.parent)
		Files.deleteIfExists(file)

		var first = true
		BufferedWriter(FileWriter(file.toFile())).use { out ->
			for(speciesClass in task.speciesClasses) {
				val part = StellarisFileString(speciesClassesFile.get())
				part.replaceStr("SpeciesArchetype", speciesClass.archetypeName)
				part.replaceStr("SpeciesClassName", speciesClass.name)
				part.replaceList("PortraitNameList", speciesClass.portraitGroupsByName.values.map {it.name})

				if(first) first = false
				else out.write("\n\n\n")
				out.write(part.toString())
			}
		}
	}


	fun generate(task: PortraitFileTask, job: PortraitJob) {
		val file = job.outModDir.resolve(task.path)
		log.info("Generating {}", file)
		Files.createDirectories(file.parent)
		Files.deleteIfExists(file)

		val fragments = ArrayList<String>()
		for(phenotype in task.portraitGroup.phenotypes) {
			val fragment = StellarisFileString(speciesPortraitsFragment.get())
			fragment.replaceStr("PortraitName", phenotype.name)
			fragment.replaceStr("PortraitFilePathRelativeToMod", phenotype.outputPath.toString())
			fragments.add(fragment.toString())
		}

		val content = StellarisFileString(speciesPortraitsFile.get())
		content.replaceStr("FirstPortraitName", task.portraitGroup.phenotypes[0].name)
		content.replaceList("PortraitFileList", fragments, quoteIfNecessary = false)
		content.replaceStr("PortraitGroupName", task.portraitGroup.name)
		content.replaceList("PortraitNameList", task.portraitGroup.phenotypes.map {it.name})

		BufferedWriter(FileWriter(file.toFile())).use {out ->
			out.write(content.toString())
		}
	}
}