# impatient-stellaris

A program that generates portrait mods for Stellaris.
Put a bunch of images (of any reasonably type - PNG, JPEG, etc.) in some folders, then run this tool,
and you'll get a mod that adds all the images to the game as portraits.

## Terminology

This program works with several Stellaris concepts:

* **Species Archetype**: defines how many traits a species can have, and which traits are available.
Built-in archetypes are BIOLOGICAL, MACHINE, LITHOID, etc.
  
* **Species Class**: a group of possible portraits that all have the same species archetype.
In the empire designer, species classes are in a sidebar on the left of the Appearance screen.
Examples are Humanoid and Mammalian.

* **Portrait Group**: a list of possible portraits a species can have.
In the empire designer, on the Appearance page, each picture you can click is a species group.
  
* **Phenotype**: a portrait group has 1 or more of these.
In the empire designer, on the Ruler page, the Phenotype box lets you switch between the various phenotypes for the portrait group you selected.
(Each phenotype is a single image.)


## File Organization

To generate a portrait mod, you need to set up an input folder with the following structure:

* The name of the input folder is the mod name (e.g. my_custom_portraits).

* The folders inside the input folder represent species archetypes (e.g. BIOLOGICAL).

* The folders inside archetype folders represent species classes (e.g. Humanoid2).

* Inside a species class folder,
	* An image file represents a species group with a single phenotype.
	* A folder represents a species group with multiple phenotypes.

* Inside a species group folder, each image file is a phenotype.

### Unique Names
In general, all names need to be unique within their category.
So you can't have 2 species classes with the same name (even if they have different archetypes),
or 2 portrait groups with the same name, or 2 phenotypes.
Even outside your mod, conflicts are likely a problem:
having a portrait name the same as a name in a different mod, or the same as a name already in the game, may cause problems.
However, names do not have to be unique between categories.
So it shouldn't be a problem to have a species class with the same name as a species group, etc.

### Special Characters
Names of archetypes, groups, etc. should ideally consist only of letters, numbers, and underscores.
You can try adding special or non-English characters (and this program will put quotes around such names),
but you may run into trouble. Putting "double quotes" in names definitely won't work.