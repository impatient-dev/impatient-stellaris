package imp.stellaris.portrait

import imp.util.ClassResources

/**Loads 1 file from src/main/resources, then caches it in memory. Not thread-safe.*/
class CachedStringResource (
	val path: String
) {
	private var string: String? = null

	fun get(): String {
		if(string == null)
			string = ClassResources.loadAsString(CachedStringResource::class, path)
		return string!!
	}
}