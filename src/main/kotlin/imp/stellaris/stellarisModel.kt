package imp.stellaris

import java.lang.IllegalArgumentException


enum class EmpireAuthority {
	Democratic,
	Oligarchic,
	Dictatorial,
	Imperial,
	Corporate,
	Hive_Mind,
	Machine
}


enum class EthicType (
	val allowsFanatic: Boolean = true,
) {
	Gestalt(false),
	Authoritarian,
	Egalitarian,
	Materialist,
	Militarist,
	Pacifist,
	Spiritualist,
	Xenophile,
	Xenophobe;

	fun opposite(): EthicType? = when(this) {
		Gestalt -> null
		Authoritarian -> Egalitarian
		Egalitarian -> Authoritarian
		Materialist -> Spiritualist
		Militarist -> Pacifist
		Pacifist -> Militarist
		Spiritualist -> Materialist
		Xenophile -> Xenophobe
		Xenophobe -> Xenophile
	}
}

data class Ethic (
	val type: EthicType,
	val fanatic: Boolean = false
) {
	init {
		if(fanatic && !type.allowsFanatic)
			throw IllegalArgumentException("$type cannot be fanatic.")
	}
}


enum class EmpireOrigin {
	Galactic_Doorstep,
	Prosperous_Unification,
	Lost_Colony,
	Calamitous_Birth,
	Mechanist,
	Synretic_Evolution,
	Tree_of_Life,
	Resource_Consolidation,
	Life_Seeded,
	Post_Apolalyptic,
	Remnants,
	Common_Ground,
	Hegemon,
	Doomsday,
	On_the_Shoulders_of_Giants,
	Scion,
	Shattered_Ring,
	Void_Dwellers,
	Necrophage,
}


enum class SpeciesMind {
	Regular,
	Hive_Mind,
	Machine,
}


enum class AiPersonality (
	val mind: SpeciesMind
) {
	Hegemonic_Imperialists(SpeciesMind.Regular),
	Federation_Builders(SpeciesMind.Regular),
	Democratic_Crusaders(SpeciesMind.Regular),
	Evangelizing_Zealots(SpeciesMind.Regular),
	Migratory_Flock(SpeciesMind.Regular),
	Spiritual_Seekers(SpeciesMind.Regular),
	Decadent_Hierarchy(SpeciesMind.Regular),
	Slaving_Despots(SpeciesMind.Regular),
	Erudite_Explorers(SpeciesMind.Regular),
	Fanatical_Purifiers(SpeciesMind.Regular),
	Metalheads(SpeciesMind.Regular),
	Harmonious_Collective(SpeciesMind.Regular),
	Peaceful_Traders(SpeciesMind.Regular),
	Xenophobic_Isolationists(SpeciesMind.Regular),
	Ruthless_Capitalists(SpeciesMind.Regular),
	Honorbound_Warriors(SpeciesMind.Regular),
	Fanatical_Befrienders(SpeciesMind.Regular),

	Hive_Mind(SpeciesMind.Hive_Mind),
	Devouring_Swarm(SpeciesMind.Hive_Mind),

	Machine_Intelligence(SpeciesMind.Machine),
	Driven_Assimilators(SpeciesMind.Machine),
	Determined_Exterminators(SpeciesMind.Machine),
	Rogue_Servitors(SpeciesMind.Machine),
}


enum class SpeciesTrait (
	val cost: Int,
) {
	Adaptive(2),
	Extremely_Adaptive(4),
	Nonadaptive(-2),
	Agrarian(2),
	Charismatic(2),
	Repugnant(2),
	Communal(1),
	Solitary(1),
	Conformists(2),
	Deviants(-1),
	Conservationist(1),
	Wasteful(-1),
	Docile(2),
	Unruly(-2),
	Enduring(1),
	Venerable(4),
	Fleeting(-1),
	Industrious(2),
	Ingenious(2),
	Intelligent(2),
	Natural_Engineers(1),
	Natural_Physicists(1),
	Natural_Sociologists(1),
	Nomadic(1),
	Sedentary(-1),
	Quick_Learners(1),
	Slow_earners(-1),
	Rapid_Breeders(2),
	Slow_Breeders(-2),
	Resilient(1),
	Strong(1),
	Very_String(3),
	Weak(-1),
	Talented(1),
	Thrifty(2),
	Traditional(1),
	Quarrelsome(-1),
	Decadent(-1),

	Lithoid(0),
	Gaseous_Byproducts(2),
	Scintillating_Skin(2),
	Volatile_Excretions(2),

	//origins
	Serviles(1),
	Survivor(0),
	Void_Dweller(0),
	Necrophage(0),

	//robotic
	Domestic_Protocols(2),
	Double_Jointed(1),
	Bulky(-1),
	Durable(1),
	High_Maintenance(1),
	Efficient_Processors(3),
	Emotion_Emulators(1),
	Uncanny(-1),
	Enhanced_Memory(2),
	Harvesters(2),
	Learning_Algorithms(1),
	Repurposed_Hardware(-1),
	Logic_Engines(2),
	Loyalty_Circuits(2),
	Mass_Produced(1),
	Custom_Made(-1),
	Power_Drills(2),
	Propaganda_Machines(1),
	Recycled(2),
	Luxurious(-2),
	Streamlined_Protocols(2),
	High_Bandwidth(-2),
	Superconductive(2),
}


enum class PlanetClimateGroup {
	Dry,
	Frozen,
	Wet,
}

enum class PlanetClimate (
	group: PlanetClimateGroup? = null,
) {
	Arid(PlanetClimateGroup.Dry),
	Desert(PlanetClimateGroup.Dry),
	Savannah(PlanetClimateGroup.Dry),
	Alpine(PlanetClimateGroup.Frozen),
	Arctic(PlanetClimateGroup.Frozen),
	Tundra(PlanetClimateGroup.Frozen),
	Continental(PlanetClimateGroup.Wet),
	Ocean(PlanetClimateGroup.Wet),
	Tropical(PlanetClimateGroup.Wet),

	Tomb,
	Gaia,
	Ring,
	Habitat,
}


enum class HomeSystemType {
	TrinarySeparate,
	TrinaryCluster,
	BinarySeparate,
	BinaryCluster,
	Unary,
	UnaryMoon,
	Deneb,
	Sol,
	VoidDwellers,
	ShatteredRing,
	VoidSol,
}




data class Species (
	val group: String,
	val name: String,
	val climatePreference: PlanetClimate,
	val traits: List<SpeciesTrait>
)

data class Empire (
	val species: Species,
	val authority: EmpireAuthority,
	val ethics: ArrayList<Ethic>,
	val origin: EmpireOrigin,
	val homeSystemType: HomeSystemType,
)
