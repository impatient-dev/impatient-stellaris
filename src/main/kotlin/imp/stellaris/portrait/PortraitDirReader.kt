package imp.stellaris.portrait

import imp.stellaris.stripLastExtension
import imp.util.forEachIn
import imp.util.kdebug
import imp.util.logger
import java.nio.file.Files
import java.nio.file.Path


private val log = PortraitDirReader::class.logger


interface InputPortraitFileHandler {
	/**Called when we find an image that needs to be included in the mod (after resizing and conversion).
	 * The path is relative to the top-level directory (the one that contains species archetypes).*/
	fun onImage(inputPath: Path, portraitGroup: String, speciesClass: String, speciesArchetype: String)
}


private class DuplicateNameDetector {
	val speciesClassDirsByName = HashMap<String, Path>()
	val portraitGroupPathsByName = HashMap<String, Path>()
	val portraitFilesByName = HashMap<String, Path>()

	fun addSpeciesClassAssertNotDuplicate(name: String, path: Path) {
		speciesClassDirsByName.compute(name) {_, existing ->
			if(existing == null) path
			else throw Exception("Duplicate species classes: $existing and $path")
		}
	}

	fun addPortraitGroupAssertNotDuplicate(name: String, path: Path) {
		portraitGroupPathsByName.compute(name) {_, existing ->
			if(existing == null) path
			else throw Exception("Duplicate portrait groups: $existing and $path")
		}
	}

	fun addPortraitFileAssertNotDuplicate(name: String, path: Path) {
		portraitFilesByName.compute(name) {_, existing ->
			if(existing == null) path
			else throw Exception("Duplicate portrait file names: $existing and $path")
		}
	}
}


object PortraitDirReader {
	/**Processes a directory, where each folder represents an image.
	 * This will process the subdirectories, etc., finding every input file that needs to be handled and sending it to the handler.
	 * If there are any duplicate names (of species classes, portrait groups, or portraits), this function will throw an exception.*/
	fun processArchetypesDir(dir: Path, handler: InputPortraitFileHandler) {
		log.debug("Looking for species archetypes in {}", dir.toAbsolutePath())
		val dup = DuplicateNameDetector()

		dir.forEachIn { path ->
			if(Files.isDirectory(path)) {
				val speciesArchetype = path.fileName.toString()
				processSpeciesClassesDir(path, speciesArchetype, dir, dup, handler)
			} else
				log.kdebug { "Ignoring - should be a species archetype directory: ${dir.relativize(path)}" }
		}
	}


	private fun processSpeciesClassesDir(dir: Path, speciesArchetype: String, topDir: Path, dup: DuplicateNameDetector, handler: InputPortraitFileHandler) {
		log.kdebug { "Looking for species classes in ${topDir.relativize(dir)}" }

		dir.forEachIn {path ->
			if(Files.isDirectory(path)) {
				val speciesClass = path.fileName.toString()
				dup.addSpeciesClassAssertNotDuplicate(speciesClass, path)
				processPortraitGroupsDir(path, speciesClass, speciesArchetype, topDir, dup, handler)
			} else
				log.kdebug { "Ignoring - should be a species group directory: ${topDir.relativize(path)}" }
		}
	}


	private fun processPortraitGroupsDir(dir: Path, speciesClass: String, speciesArchetype: String, topDir: Path, dup: DuplicateNameDetector, handler: InputPortraitFileHandler) {
		log.kdebug { "Looking for portrait groups in ${topDir.relativize(dir)}" }
		dir.forEachIn {path ->
			val relative = topDir.relativize(path)
			if(Files.isDirectory(path)) {
				val fileName = path.fileName.toString()
				dup.addPortraitGroupAssertNotDuplicate(fileName, relative)
				processPhenotypesDir(path, fileName, speciesClass, speciesArchetype, topDir, dup, handler)
			} else {
				val portraitGroup = path.fileName.toString().stripLastExtension()
				dup.addPortraitFileAssertNotDuplicate(portraitGroup, relative)
				handler.onImage(relative, portraitGroup, speciesClass, speciesArchetype)
			}
		}
	}


	private fun processPhenotypesDir(dir: Path, portraitGroup: String, speciesClass: String, speciesArchetype: String, topDir: Path,
	                                 dup: DuplicateNameDetector, handler: InputPortraitFileHandler) {
		log.kdebug { "Looking for phenotypes in ${topDir.relativize(dir)}" }

		dir.forEachIn {path ->
			val relative = topDir.relativize(path)
			if(Files.isDirectory(path))
				log.debug("Ignoring - should be a portrait image file: {}", relative)
			else {
				val phenotype = path.fileName.toString().stripLastExtension()
				dup.addPortraitFileAssertNotDuplicate(phenotype, relative)
				handler.onImage(relative, portraitGroup, speciesClass, speciesArchetype)
			}
		}
	}
}