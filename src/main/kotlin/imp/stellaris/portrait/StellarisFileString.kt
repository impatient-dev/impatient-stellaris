package imp.stellaris.portrait

import imp.util.logger
import java.util.regex.Pattern

private val log = StellarisFileString::class.logger

private val newlineChars = setOf('\r', '\n')

/**Regex for text that can be inserted into Stellaris mod files as-is, without quoting.*/
private val regexNoQuotesNeeded = Pattern.compile("[\\p{Alnum}_]+")

/**Wraps a string that will be inserted into a Stellaris mod file.
 * The string may contain variables like $PortraitName$, which can be replaced by methods in this class.
 * All such variables much be replaced before you can call toString() to get a string that can be inserted into a mod file.
 * Warning: values currently can't be multiline - the format will be botched horribly.*/
class StellarisFileString (
	private var text: String,
) {
	/**Replaces all instances of a variable with some value.
	 * @param varName the variable name, without dollar signs; PortraitCount will be turned into $PortraitCount$.*/
	fun replaceStr(varName: String, value: String, quoteIfNecessary: Boolean = true, failIfNonExistent: Boolean = true) {
		val key = "$" + varName + "$"
		if(failIfNonExistent && !text.contains(key))
			throw IllegalStateException("Variable not found: $key")
		text = text.replace(key, if(quoteIfNecessary) quotedIfNecessary(value) else value)
	}


	/**Replaces all instances of a variable with a list of values.
	 * The values may be placed on separate lines, or on the same line separated by spaces.*/
	fun replaceList(varName: String, values: List<String>, quoteIfNecessary: Boolean = true, failIfNonExistent: Boolean = true) {
		val key = "$" + varName + "$"
		if(failIfNonExistent && !text.contains(key))
			throw IllegalStateException("Variable not found: $key")
		val replacements = values.map {if(quoteIfNecessary) quotedIfNecessary(it) else it}
		val replacementChars = replacements.sumOf { it.length } + replacements.size - 1
		val considerMultiline = replacementChars >= 30

		while(true) {
			val start = text.indexOf(key)
			if(start == -1)
				return
			val end = start.plus(key.length)
			if(considerMultiline && canDoMultiLineReplacement(text, start, end))
				text = doMultiLineReplacement(text, start, end, replacements)
			else
				text = text.replaceRange(start, end, replacements.joinToString(" "))
		}
	}

	/**Fails if not all variables have been replaced.*/
	override fun toString(): String {
		val start = text.indexOf('$')
		if(start == -1)
			return text
		var end = text.indexOf('$', start + 1)
		log.debug("Not all $ characters replaced: {}", text)
		if(end == -1) throw IllegalStateException("Not all $ characters were relaced.")
		else throw IllegalStateException("Variable was not replaced: ${text.substring(start, end + 1)}")
	}


	private fun quotedIfNecessary(str: String): String = if(regexNoQuotesNeeded.matcher(str).matches()) str else '"' + str + '"'

	/**Returns true if the line containing this region has only whitespace before start, and has nothing at all after end.*/
	private fun canDoMultiLineReplacement(text: String, start: Int, end: Int): Boolean {
		if(text.length > end && !newlineChars.contains(text[end]))
			return false
		return startOfLinePastWhitespaceOnly(text, start) != null
	}

	private fun doMultiLineReplacement(text: String, start: Int, end: Int, replacements: List<String>): String {
		val indent = text.substring(startOfLinePastWhitespaceOnly(text, start)!!, start)
		return text.replaceRange(start, end, replacements.joinToString("\n$indent"))
	}
}


/**Starts at the provided position in the string and moves towards the start (0).
 * If this function reaches the start of the line, and all characters it encounters to that point are whitespace, returns the position of the first character on the line.
 * If there are non-whitespace characters before the start of the line, returns null.
 * Public for testing only.*/
fun startOfLinePastWhitespaceOnly(str: String, pos: Int): Int? {
	var i = pos
	while(true) {
		if(i == 0)
			return 0
		val prev = str[i - 1]
		if(newlineChars.contains(prev))
			return i
		if(!prev.isWhitespace())
			return null
		i--
	}
}