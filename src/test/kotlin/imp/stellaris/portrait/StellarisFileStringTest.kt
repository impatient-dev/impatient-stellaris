package imp.stellaris.portrait

import org.junit.Assert.assertEquals
import org.junit.Test

class StellarisFileStringTest {
	@Test fun testLineStartFinder() {
		assertEquals(0, startOfLinePastWhitespaceOnly("", 0))
		assertEquals(0, startOfLinePastWhitespaceOnly(" a", 0))
		assertEquals(0, startOfLinePastWhitespaceOnly(" a", 1))
		assertEquals(1, startOfLinePastWhitespaceOnly("\na", 1))
		assertEquals(2, startOfLinePastWhitespaceOnly("\r\n  a", 4))
	}
}