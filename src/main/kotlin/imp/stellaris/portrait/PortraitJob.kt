package imp.stellaris.portrait

import java.nio.file.Path
import java.util.regex.Pattern

private val acceptableModNameRegex = Pattern.compile("[\\p{Alnum}_-]+")

class PortraitJob (
	/**The top-level input directory, which contains other directories and (eventually) images.*/
	val inDir: Path,
	/**The directory where output goes. This directory is the generated mod; it's not the directory that contains the mod.*/
	val outModDir: Path,
) {
	/**Derived from the input directory.*/
	val modName = inDir.fileName.toString()

	init {
		if(!acceptableModNameRegex.matcher(modName).matches())
			throw Exception("Illegal characters in mod name: $modName")
	}
}


/**Task to convert an image to one of a different size/format.*/
class ImageConvertTask (
	/**Relative to the input directory.*/
	val src: Path,
	/**Relative to the output directory.*/
	val dest: Path,
	/**In pixels.*/
	val desiredHeight: Int = 350,
)

/**Task to write a common/species_classes/something.txt file.*/
class SpeciesClassesFileTask (
	/**The file to write, relative to the output directory.*/
	val path: Path,
	val speciesClasses: List<SpeciesClassBasicInfo>,
)

/**Task to write all portraits for a portrait group to a gfx/portraits/portraits/something.txt file.*/
class PortraitFileTask (
	val portraitGroup: PortraitGroupBasicInfo,
	/**The file to write, relative to the output directory.*/
	val path: Path,
)


class SpeciesClassBasicInfo (
	val name: String,
	val archetypeName: String,
) {
	val portraitGroupsByName = HashMap<String, PortraitGroupBasicInfo>()
//	val portraitNames = ArrayList<String>()
}

class PortraitGroupBasicInfo (
	/**The name of this portrait (group). If there is more than 1 phenotype for this portrait, they will have different names.*/
	val name: String,
) {
	val phenotypes = ArrayList<PortraitPhenotypeBasicInfo>()
}

data class PortraitPhenotypeBasicInfo (
	/**Possibly the same as the portrait name.*/
	val name: String,
	/**Relative to the input directory.*/
	val inputPath: Path,
	/**Relative to the output (mod) directory.*/
	val outputPath: Path,
)