YOUR_ARCHETYPE_NAME = {
	inherit_traits_from = { BIOLOGICAL MACHINE } #species with this archetype can have traits from biological (regular) species, and machine traits
	species_trait_points = 2
	species_max_traits = 5
}


# Note: you can also use variables like this:
#@points = 2
#species_trait_points = @points

# Note: useful species archetypes are BIOLOGICAL ROBOT MACHINE PRESAPIENT LITHOID
# MACHINE is for stuff in the Synthetic Dawn Story Pack; ROBOT is something else and can't be played until after the game starts.

# Note: instead of writing species_trait_points, you can do
#inherit_trait_points_from = BIOLOGICAL